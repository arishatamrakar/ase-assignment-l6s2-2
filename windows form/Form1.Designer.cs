﻿namespace windows_form
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.openFileButton = new System.Windows.Forms.Button();
            this.fileChooserDialog = new System.Windows.Forms.OpenFileDialog();
            this.mainTabControl = new System.Windows.Forms.TabControl();
            this.headersPage = new System.Windows.Forms.TabPage();
            this.weightValue = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.vo2maxValue = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.startDelayValue = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.restHRValue = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.maxHRValue = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.activeLimitValue = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.timer3Value = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.timer2Value = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.timer1Value = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.lowerLimit3Value = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.upperLimit3Value = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.lowerLimit2Value = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.upperLimit2Value = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.lowerLimit1Value = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.upperLimit1Value = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.intervalValue = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lengthValue = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.startTimeValue = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.dateValue = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.smodeValue = new System.Windows.Forms.Label();
            this.smodeLabel = new System.Windows.Forms.Label();
            this.monitorValue = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.versionValue = new System.Windows.Forms.Label();
            this.versionLabel = new System.Windows.Forms.Label();
            this.hrDataPage = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.minPowerValue = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.avgPowerValue = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.maxPowerValue = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.minAltitudeValue = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.avgAltitudeValue = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.maxAltitudeValue = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.minCadenceValue = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.avgCadenceValue = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.maxCadenceValue = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.minSpeedValue = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.avgSpeedValue = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.maxSpeedValue = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.minHeartRateValue = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.avgHeartRateValue = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.maxHeartRateValue = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.distanceCoveredValue = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.hrDataGrid = new System.Windows.Forms.DataGridView();
            this.graphPage = new System.Windows.Forms.TabPage();
            this.label2 = new System.Windows.Forms.Label();
            this.tssVal = new System.Windows.Forms.Label();
            this.ifVal = new System.Windows.Forms.Label();
            this.avgPowVal = new System.Windows.Forms.Label();
            this.avgAltVal = new System.Windows.Forms.Label();
            this.avgCadVal = new System.Windows.Forms.Label();
            this.avgSpdVal = new System.Windows.Forms.Label();
            this.avgHRVal = new System.Windows.Forms.Label();
            this.npVal = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.tssLabel = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.intesityLabel = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.zoomOutBtn = new System.Windows.Forms.Button();
            this.powerCheck = new System.Windows.Forms.CheckBox();
            this.altitudeCheck = new System.Windows.Forms.CheckBox();
            this.cadenceCheck = new System.Windows.Forms.CheckBox();
            this.speedCheck = new System.Windows.Forms.CheckBox();
            this.heartRateCheck = new System.Windows.Forms.CheckBox();
            this.zgc = new ZedGraph.ZedGraphControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.changeFolderPath = new System.Windows.Forms.Button();
            this.calenDataDetails = new System.Windows.Forms.DataGridView();
            this.startTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.length = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.interval = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.load_file = new System.Windows.Forms.DataGridViewLinkColumn();
            this.filePath = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.monthCalen = new System.Windows.Forms.MonthCalendar();
            this.intervalDetectionGrid = new System.Windows.Forms.TabPage();
            this.intervalDetectSummaryPanel = new System.Windows.Forms.Panel();
            this.intervalsGrid = new System.Windows.Forms.DataGridView();
            this.helpProvider1 = new System.Windows.Forms.HelpProvider();
            this.mainTabControl.SuspendLayout();
            this.headersPage.SuspendLayout();
            this.hrDataPage.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.hrDataGrid)).BeginInit();
            this.graphPage.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.calenDataDetails)).BeginInit();
            this.intervalDetectionGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.intervalsGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // openFileButton
            // 
            this.openFileButton.Location = new System.Drawing.Point(1043, 26);
            this.openFileButton.Name = "openFileButton";
            this.openFileButton.Size = new System.Drawing.Size(135, 32);
            this.openFileButton.TabIndex = 0;
            this.openFileButton.Text = "Choose File";
            this.openFileButton.UseVisualStyleBackColor = true;
            this.openFileButton.Click += new System.EventHandler(this.openFileButton_Click);
            // 
            // fileChooserDialog
            // 
            this.fileChooserDialog.FileName = "openFileDialog1";
            // 
            // mainTabControl
            // 
            this.mainTabControl.Controls.Add(this.headersPage);
            this.mainTabControl.Controls.Add(this.hrDataPage);
            this.mainTabControl.Controls.Add(this.graphPage);
            this.mainTabControl.Controls.Add(this.tabPage1);
            this.mainTabControl.Controls.Add(this.intervalDetectionGrid);
            this.mainTabControl.Location = new System.Drawing.Point(2, 64);
            this.mainTabControl.Name = "mainTabControl";
            this.mainTabControl.SelectedIndex = 0;
            this.mainTabControl.Size = new System.Drawing.Size(1191, 528);
            this.mainTabControl.TabIndex = 1;
            // 
            // headersPage
            // 
            this.headersPage.BackColor = System.Drawing.Color.Teal;
            this.headersPage.Controls.Add(this.weightValue);
            this.headersPage.Controls.Add(this.label52);
            this.headersPage.Controls.Add(this.vo2maxValue);
            this.headersPage.Controls.Add(this.label34);
            this.headersPage.Controls.Add(this.startDelayValue);
            this.headersPage.Controls.Add(this.label36);
            this.headersPage.Controls.Add(this.restHRValue);
            this.headersPage.Controls.Add(this.label38);
            this.headersPage.Controls.Add(this.maxHRValue);
            this.headersPage.Controls.Add(this.label40);
            this.headersPage.Controls.Add(this.activeLimitValue);
            this.headersPage.Controls.Add(this.label42);
            this.headersPage.Controls.Add(this.timer3Value);
            this.headersPage.Controls.Add(this.label18);
            this.headersPage.Controls.Add(this.timer2Value);
            this.headersPage.Controls.Add(this.label20);
            this.headersPage.Controls.Add(this.timer1Value);
            this.headersPage.Controls.Add(this.label22);
            this.headersPage.Controls.Add(this.lowerLimit3Value);
            this.headersPage.Controls.Add(this.label24);
            this.headersPage.Controls.Add(this.upperLimit3Value);
            this.headersPage.Controls.Add(this.label26);
            this.headersPage.Controls.Add(this.lowerLimit2Value);
            this.headersPage.Controls.Add(this.label28);
            this.headersPage.Controls.Add(this.upperLimit2Value);
            this.headersPage.Controls.Add(this.label30);
            this.headersPage.Controls.Add(this.lowerLimit1Value);
            this.headersPage.Controls.Add(this.label32);
            this.headersPage.Controls.Add(this.upperLimit1Value);
            this.headersPage.Controls.Add(this.label10);
            this.headersPage.Controls.Add(this.intervalValue);
            this.headersPage.Controls.Add(this.label12);
            this.headersPage.Controls.Add(this.lengthValue);
            this.headersPage.Controls.Add(this.label14);
            this.headersPage.Controls.Add(this.startTimeValue);
            this.headersPage.Controls.Add(this.label16);
            this.headersPage.Controls.Add(this.dateValue);
            this.headersPage.Controls.Add(this.label6);
            this.headersPage.Controls.Add(this.smodeValue);
            this.headersPage.Controls.Add(this.smodeLabel);
            this.headersPage.Controls.Add(this.monitorValue);
            this.headersPage.Controls.Add(this.label4);
            this.headersPage.Controls.Add(this.versionValue);
            this.headersPage.Controls.Add(this.versionLabel);
            this.headersPage.Location = new System.Drawing.Point(4, 22);
            this.headersPage.Name = "headersPage";
            this.headersPage.Padding = new System.Windows.Forms.Padding(3);
            this.headersPage.Size = new System.Drawing.Size(1183, 502);
            this.headersPage.TabIndex = 0;
            this.headersPage.Text = "Header Details";
            // 
            // weightValue
            // 
            this.weightValue.AutoSize = true;
            this.weightValue.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.weightValue.Location = new System.Drawing.Point(538, 142);
            this.weightValue.Name = "weightValue";
            this.weightValue.Size = new System.Drawing.Size(22, 22);
            this.weightValue.TabIndex = 43;
            this.weightValue.Text = "--";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.Location = new System.Drawing.Point(411, 142);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(70, 22);
            this.label52.TabIndex = 42;
            this.label52.Text = "Weight :";
            // 
            // vo2maxValue
            // 
            this.vo2maxValue.AutoSize = true;
            this.vo2maxValue.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vo2maxValue.Location = new System.Drawing.Point(538, 117);
            this.vo2maxValue.Name = "vo2maxValue";
            this.vo2maxValue.Size = new System.Drawing.Size(22, 22);
            this.vo2maxValue.TabIndex = 41;
            this.vo2maxValue.Text = "--";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(411, 117);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(86, 22);
            this.label34.TabIndex = 40;
            this.label34.Text = "VO2 Max :";
            // 
            // startDelayValue
            // 
            this.startDelayValue.AutoSize = true;
            this.startDelayValue.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.startDelayValue.Location = new System.Drawing.Point(538, 92);
            this.startDelayValue.Name = "startDelayValue";
            this.startDelayValue.Size = new System.Drawing.Size(22, 22);
            this.startDelayValue.TabIndex = 39;
            this.startDelayValue.Text = "--";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(411, 92);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(99, 22);
            this.label36.TabIndex = 38;
            this.label36.Text = "Start Delay :";
            // 
            // restHRValue
            // 
            this.restHRValue.AutoSize = true;
            this.restHRValue.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.restHRValue.Location = new System.Drawing.Point(538, 68);
            this.restHRValue.Name = "restHRValue";
            this.restHRValue.Size = new System.Drawing.Size(22, 22);
            this.restHRValue.TabIndex = 37;
            this.restHRValue.Text = "--";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(411, 68);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(76, 22);
            this.label38.TabIndex = 36;
            this.label38.Text = "Rest HR :";
            // 
            // maxHRValue
            // 
            this.maxHRValue.AutoSize = true;
            this.maxHRValue.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.maxHRValue.Location = new System.Drawing.Point(538, 43);
            this.maxHRValue.Name = "maxHRValue";
            this.maxHRValue.Size = new System.Drawing.Size(22, 22);
            this.maxHRValue.TabIndex = 35;
            this.maxHRValue.Text = "--";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(411, 43);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(76, 22);
            this.label40.TabIndex = 34;
            this.label40.Text = "Max HR :";
            // 
            // activeLimitValue
            // 
            this.activeLimitValue.AutoSize = true;
            this.activeLimitValue.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.activeLimitValue.Location = new System.Drawing.Point(538, 18);
            this.activeLimitValue.Name = "activeLimitValue";
            this.activeLimitValue.Size = new System.Drawing.Size(22, 22);
            this.activeLimitValue.TabIndex = 33;
            this.activeLimitValue.Text = "--";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(411, 18);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(104, 22);
            this.label42.TabIndex = 32;
            this.label42.Text = "Active Limit :";
            // 
            // timer3Value
            // 
            this.timer3Value.AutoSize = true;
            this.timer3Value.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timer3Value.Location = new System.Drawing.Point(149, 388);
            this.timer3Value.Name = "timer3Value";
            this.timer3Value.Size = new System.Drawing.Size(22, 22);
            this.timer3Value.TabIndex = 31;
            this.timer3Value.Text = "--";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(17, 388);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(74, 22);
            this.label18.TabIndex = 30;
            this.label18.Text = "Timer 3 :";
            // 
            // timer2Value
            // 
            this.timer2Value.AutoSize = true;
            this.timer2Value.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timer2Value.Location = new System.Drawing.Point(149, 363);
            this.timer2Value.Name = "timer2Value";
            this.timer2Value.Size = new System.Drawing.Size(22, 22);
            this.timer2Value.TabIndex = 29;
            this.timer2Value.Text = "--";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(17, 363);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(74, 22);
            this.label20.TabIndex = 28;
            this.label20.Text = "Timer 2 :";
            // 
            // timer1Value
            // 
            this.timer1Value.AutoSize = true;
            this.timer1Value.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timer1Value.Location = new System.Drawing.Point(149, 339);
            this.timer1Value.Name = "timer1Value";
            this.timer1Value.Size = new System.Drawing.Size(22, 22);
            this.timer1Value.TabIndex = 27;
            this.timer1Value.Text = "--";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(17, 339);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(74, 22);
            this.label22.TabIndex = 26;
            this.label22.Text = "Timer 1 :";
            // 
            // lowerLimit3Value
            // 
            this.lowerLimit3Value.AutoSize = true;
            this.lowerLimit3Value.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lowerLimit3Value.Location = new System.Drawing.Point(149, 314);
            this.lowerLimit3Value.Name = "lowerLimit3Value";
            this.lowerLimit3Value.Size = new System.Drawing.Size(22, 22);
            this.lowerLimit3Value.TabIndex = 25;
            this.lowerLimit3Value.Text = "--";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(17, 314);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(118, 22);
            this.label24.TabIndex = 24;
            this.label24.Text = "Lower Limit 3 :";
            // 
            // upperLimit3Value
            // 
            this.upperLimit3Value.AutoSize = true;
            this.upperLimit3Value.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.upperLimit3Value.Location = new System.Drawing.Point(149, 289);
            this.upperLimit3Value.Name = "upperLimit3Value";
            this.upperLimit3Value.Size = new System.Drawing.Size(22, 22);
            this.upperLimit3Value.TabIndex = 23;
            this.upperLimit3Value.Text = "--";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(17, 289);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(117, 22);
            this.label26.TabIndex = 22;
            this.label26.Text = "Upper Limit 3 :";
            // 
            // lowerLimit2Value
            // 
            this.lowerLimit2Value.AutoSize = true;
            this.lowerLimit2Value.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lowerLimit2Value.Location = new System.Drawing.Point(149, 264);
            this.lowerLimit2Value.Name = "lowerLimit2Value";
            this.lowerLimit2Value.Size = new System.Drawing.Size(22, 22);
            this.lowerLimit2Value.TabIndex = 21;
            this.lowerLimit2Value.Text = "--";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(17, 264);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(118, 22);
            this.label28.TabIndex = 20;
            this.label28.Text = "Lower Limit 2 :";
            // 
            // upperLimit2Value
            // 
            this.upperLimit2Value.AutoSize = true;
            this.upperLimit2Value.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.upperLimit2Value.Location = new System.Drawing.Point(149, 240);
            this.upperLimit2Value.Name = "upperLimit2Value";
            this.upperLimit2Value.Size = new System.Drawing.Size(22, 22);
            this.upperLimit2Value.TabIndex = 19;
            this.upperLimit2Value.Text = "--";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(17, 240);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(117, 22);
            this.label30.TabIndex = 18;
            this.label30.Text = "Upper Limit 2 :";
            // 
            // lowerLimit1Value
            // 
            this.lowerLimit1Value.AutoSize = true;
            this.lowerLimit1Value.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lowerLimit1Value.Location = new System.Drawing.Point(149, 215);
            this.lowerLimit1Value.Name = "lowerLimit1Value";
            this.lowerLimit1Value.Size = new System.Drawing.Size(22, 22);
            this.lowerLimit1Value.TabIndex = 17;
            this.lowerLimit1Value.Text = "--";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(17, 215);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(118, 22);
            this.label32.TabIndex = 16;
            this.label32.Text = "Lower Limit 1 :";
            // 
            // upperLimit1Value
            // 
            this.upperLimit1Value.AutoSize = true;
            this.upperLimit1Value.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.upperLimit1Value.Location = new System.Drawing.Point(149, 191);
            this.upperLimit1Value.Name = "upperLimit1Value";
            this.upperLimit1Value.Size = new System.Drawing.Size(22, 22);
            this.upperLimit1Value.TabIndex = 15;
            this.upperLimit1Value.Text = "--";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(17, 191);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(117, 22);
            this.label10.TabIndex = 14;
            this.label10.Text = "Upper Limit 1 :";
            // 
            // intervalValue
            // 
            this.intervalValue.AutoSize = true;
            this.intervalValue.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.intervalValue.Location = new System.Drawing.Point(149, 166);
            this.intervalValue.Name = "intervalValue";
            this.intervalValue.Size = new System.Drawing.Size(22, 22);
            this.intervalValue.TabIndex = 13;
            this.intervalValue.Text = "--";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(17, 166);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(75, 22);
            this.label12.TabIndex = 12;
            this.label12.Text = "Interval :";
            // 
            // lengthValue
            // 
            this.lengthValue.AutoSize = true;
            this.lengthValue.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lengthValue.Location = new System.Drawing.Point(149, 142);
            this.lengthValue.Name = "lengthValue";
            this.lengthValue.Size = new System.Drawing.Size(22, 22);
            this.lengthValue.TabIndex = 11;
            this.lengthValue.Text = "--";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(17, 142);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(72, 22);
            this.label14.TabIndex = 10;
            this.label14.Text = "Length : ";
            // 
            // startTimeValue
            // 
            this.startTimeValue.AutoSize = true;
            this.startTimeValue.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.startTimeValue.Location = new System.Drawing.Point(149, 117);
            this.startTimeValue.Name = "startTimeValue";
            this.startTimeValue.Size = new System.Drawing.Size(22, 22);
            this.startTimeValue.TabIndex = 9;
            this.startTimeValue.Text = "--";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(17, 117);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(94, 22);
            this.label16.TabIndex = 8;
            this.label16.Text = "Start Time :";
            // 
            // dateValue
            // 
            this.dateValue.AutoSize = true;
            this.dateValue.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateValue.Location = new System.Drawing.Point(149, 92);
            this.dateValue.Name = "dateValue";
            this.dateValue.Size = new System.Drawing.Size(22, 22);
            this.dateValue.TabIndex = 7;
            this.dateValue.Text = "--";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(17, 92);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 22);
            this.label6.TabIndex = 6;
            this.label6.Text = "Date :";
            // 
            // smodeValue
            // 
            this.smodeValue.AutoSize = true;
            this.smodeValue.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smodeValue.Location = new System.Drawing.Point(149, 67);
            this.smodeValue.Name = "smodeValue";
            this.smodeValue.Size = new System.Drawing.Size(22, 22);
            this.smodeValue.TabIndex = 5;
            this.smodeValue.Text = "--";
            // 
            // smodeLabel
            // 
            this.smodeLabel.AutoSize = true;
            this.smodeLabel.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smodeLabel.Location = new System.Drawing.Point(17, 67);
            this.smodeLabel.Name = "smodeLabel";
            this.smodeLabel.Size = new System.Drawing.Size(70, 22);
            this.smodeLabel.TabIndex = 4;
            this.smodeLabel.Text = "SMode :";
            // 
            // monitorValue
            // 
            this.monitorValue.AutoSize = true;
            this.monitorValue.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.monitorValue.Location = new System.Drawing.Point(149, 43);
            this.monitorValue.Name = "monitorValue";
            this.monitorValue.Size = new System.Drawing.Size(22, 22);
            this.monitorValue.TabIndex = 3;
            this.monitorValue.Text = "--";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(17, 43);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 22);
            this.label4.TabIndex = 2;
            this.label4.Text = "Monitor :";
            // 
            // versionValue
            // 
            this.versionValue.AutoSize = true;
            this.versionValue.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.versionValue.Location = new System.Drawing.Point(149, 18);
            this.versionValue.Name = "versionValue";
            this.versionValue.Size = new System.Drawing.Size(22, 22);
            this.versionValue.TabIndex = 1;
            this.versionValue.Text = "--";
            // 
            // versionLabel
            // 
            this.versionLabel.AutoSize = true;
            this.versionLabel.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.versionLabel.Location = new System.Drawing.Point(17, 18);
            this.versionLabel.Name = "versionLabel";
            this.versionLabel.Size = new System.Drawing.Size(73, 22);
            this.versionLabel.TabIndex = 0;
            this.versionLabel.Text = "Version :";
            // 
            // hrDataPage
            // 
            this.hrDataPage.BackColor = System.Drawing.Color.Teal;
            this.hrDataPage.Controls.Add(this.panel1);
            this.hrDataPage.Controls.Add(this.hrDataGrid);
            this.hrDataPage.Location = new System.Drawing.Point(4, 22);
            this.hrDataPage.Name = "hrDataPage";
            this.hrDataPage.Padding = new System.Windows.Forms.Padding(3);
            this.hrDataPage.Size = new System.Drawing.Size(1183, 502);
            this.hrDataPage.TabIndex = 1;
            this.hrDataPage.Text = "HR Data";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.minPowerValue);
            this.panel1.Controls.Add(this.label51);
            this.panel1.Controls.Add(this.avgPowerValue);
            this.panel1.Controls.Add(this.label39);
            this.panel1.Controls.Add(this.maxPowerValue);
            this.panel1.Controls.Add(this.label43);
            this.panel1.Controls.Add(this.minAltitudeValue);
            this.panel1.Controls.Add(this.label45);
            this.panel1.Controls.Add(this.avgAltitudeValue);
            this.panel1.Controls.Add(this.label47);
            this.panel1.Controls.Add(this.maxAltitudeValue);
            this.panel1.Controls.Add(this.label49);
            this.panel1.Controls.Add(this.minCadenceValue);
            this.panel1.Controls.Add(this.label19);
            this.panel1.Controls.Add(this.avgCadenceValue);
            this.panel1.Controls.Add(this.label23);
            this.panel1.Controls.Add(this.maxCadenceValue);
            this.panel1.Controls.Add(this.label27);
            this.panel1.Controls.Add(this.minSpeedValue);
            this.panel1.Controls.Add(this.label31);
            this.panel1.Controls.Add(this.avgSpeedValue);
            this.panel1.Controls.Add(this.label35);
            this.panel1.Controls.Add(this.maxSpeedValue);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.minHeartRateValue);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.avgHeartRateValue);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.maxHeartRateValue);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.distanceCoveredValue);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(845, 6);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(332, 542);
            this.panel1.TabIndex = 1;
            // 
            // minPowerValue
            // 
            this.minPowerValue.AutoSize = true;
            this.minPowerValue.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.minPowerValue.Location = new System.Drawing.Point(204, 453);
            this.minPowerValue.Name = "minPowerValue";
            this.minPowerValue.Size = new System.Drawing.Size(22, 22);
            this.minPowerValue.TabIndex = 29;
            this.minPowerValue.Text = "--";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(20, 453);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(98, 22);
            this.label51.TabIndex = 30;
            this.label51.Text = "Min Power :";
            // 
            // avgPowerValue
            // 
            this.avgPowerValue.AutoSize = true;
            this.avgPowerValue.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.avgPowerValue.Location = new System.Drawing.Point(204, 424);
            this.avgPowerValue.Name = "avgPowerValue";
            this.avgPowerValue.Size = new System.Drawing.Size(22, 22);
            this.avgPowerValue.TabIndex = 27;
            this.avgPowerValue.Text = "--";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(20, 424);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(129, 22);
            this.label39.TabIndex = 28;
            this.label39.Text = "Average Power :";
            // 
            // maxPowerValue
            // 
            this.maxPowerValue.AutoSize = true;
            this.maxPowerValue.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.maxPowerValue.Location = new System.Drawing.Point(204, 393);
            this.maxPowerValue.Name = "maxPowerValue";
            this.maxPowerValue.Size = new System.Drawing.Size(22, 22);
            this.maxPowerValue.TabIndex = 25;
            this.maxPowerValue.Text = "--";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(20, 393);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(102, 22);
            this.label43.TabIndex = 26;
            this.label43.Text = "Max Power :";
            // 
            // minAltitudeValue
            // 
            this.minAltitudeValue.AutoSize = true;
            this.minAltitudeValue.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.minAltitudeValue.Location = new System.Drawing.Point(204, 364);
            this.minAltitudeValue.Name = "minAltitudeValue";
            this.minAltitudeValue.Size = new System.Drawing.Size(22, 22);
            this.minAltitudeValue.TabIndex = 23;
            this.minAltitudeValue.Text = "--";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(20, 364);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(108, 22);
            this.label45.TabIndex = 24;
            this.label45.Text = "Min Altitude :";
            // 
            // avgAltitudeValue
            // 
            this.avgAltitudeValue.AutoSize = true;
            this.avgAltitudeValue.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.avgAltitudeValue.Location = new System.Drawing.Point(204, 334);
            this.avgAltitudeValue.Name = "avgAltitudeValue";
            this.avgAltitudeValue.Size = new System.Drawing.Size(22, 22);
            this.avgAltitudeValue.TabIndex = 21;
            this.avgAltitudeValue.Text = "--";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(20, 334);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(139, 22);
            this.label47.TabIndex = 22;
            this.label47.Text = "Average Altitude :";
            // 
            // maxAltitudeValue
            // 
            this.maxAltitudeValue.AutoSize = true;
            this.maxAltitudeValue.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.maxAltitudeValue.Location = new System.Drawing.Point(204, 302);
            this.maxAltitudeValue.Name = "maxAltitudeValue";
            this.maxAltitudeValue.Size = new System.Drawing.Size(22, 22);
            this.maxAltitudeValue.TabIndex = 19;
            this.maxAltitudeValue.Text = "--";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(20, 302);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(112, 22);
            this.label49.TabIndex = 20;
            this.label49.Text = "Max Altitude :";
            // 
            // minCadenceValue
            // 
            this.minCadenceValue.AutoSize = true;
            this.minCadenceValue.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.minCadenceValue.Location = new System.Drawing.Point(204, 273);
            this.minCadenceValue.Name = "minCadenceValue";
            this.minCadenceValue.Size = new System.Drawing.Size(22, 22);
            this.minCadenceValue.TabIndex = 17;
            this.minCadenceValue.Text = "--";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(20, 273);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(114, 22);
            this.label19.TabIndex = 18;
            this.label19.Text = "Min Cadence :";
            // 
            // avgCadenceValue
            // 
            this.avgCadenceValue.AutoSize = true;
            this.avgCadenceValue.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.avgCadenceValue.Location = new System.Drawing.Point(204, 242);
            this.avgCadenceValue.Name = "avgCadenceValue";
            this.avgCadenceValue.Size = new System.Drawing.Size(22, 22);
            this.avgCadenceValue.TabIndex = 15;
            this.avgCadenceValue.Text = "--";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(20, 242);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(145, 22);
            this.label23.TabIndex = 16;
            this.label23.Text = "Average Cadence :";
            // 
            // maxCadenceValue
            // 
            this.maxCadenceValue.AutoSize = true;
            this.maxCadenceValue.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.maxCadenceValue.Location = new System.Drawing.Point(204, 213);
            this.maxCadenceValue.Name = "maxCadenceValue";
            this.maxCadenceValue.Size = new System.Drawing.Size(22, 22);
            this.maxCadenceValue.TabIndex = 13;
            this.maxCadenceValue.Text = "--";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(20, 213);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(118, 22);
            this.label27.TabIndex = 14;
            this.label27.Text = "Max Cadence :";
            // 
            // minSpeedValue
            // 
            this.minSpeedValue.AutoSize = true;
            this.minSpeedValue.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.minSpeedValue.Location = new System.Drawing.Point(204, 183);
            this.minSpeedValue.Name = "minSpeedValue";
            this.minSpeedValue.Size = new System.Drawing.Size(22, 22);
            this.minSpeedValue.TabIndex = 11;
            this.minSpeedValue.Text = "--";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(20, 183);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(95, 22);
            this.label31.TabIndex = 12;
            this.label31.Text = "Min Speed :";
            // 
            // avgSpeedValue
            // 
            this.avgSpeedValue.AutoSize = true;
            this.avgSpeedValue.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.avgSpeedValue.Location = new System.Drawing.Point(204, 156);
            this.avgSpeedValue.Name = "avgSpeedValue";
            this.avgSpeedValue.Size = new System.Drawing.Size(22, 22);
            this.avgSpeedValue.TabIndex = 9;
            this.avgSpeedValue.Text = "--";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(20, 156);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(126, 22);
            this.label35.TabIndex = 10;
            this.label35.Text = "Average Speed :";
            // 
            // maxSpeedValue
            // 
            this.maxSpeedValue.AutoSize = true;
            this.maxSpeedValue.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.maxSpeedValue.Location = new System.Drawing.Point(204, 128);
            this.maxSpeedValue.Name = "maxSpeedValue";
            this.maxSpeedValue.Size = new System.Drawing.Size(22, 22);
            this.maxSpeedValue.TabIndex = 7;
            this.maxSpeedValue.Text = "--";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(20, 128);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(99, 22);
            this.label15.TabIndex = 8;
            this.label15.Text = "Max Speed :";
            // 
            // minHeartRateValue
            // 
            this.minHeartRateValue.AutoSize = true;
            this.minHeartRateValue.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.minHeartRateValue.Location = new System.Drawing.Point(204, 97);
            this.minHeartRateValue.Name = "minHeartRateValue";
            this.minHeartRateValue.Size = new System.Drawing.Size(22, 22);
            this.minHeartRateValue.TabIndex = 5;
            this.minHeartRateValue.Text = "--";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(20, 97);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(130, 22);
            this.label11.TabIndex = 6;
            this.label11.Text = "Min Heart Rate :";
            // 
            // avgHeartRateValue
            // 
            this.avgHeartRateValue.AutoSize = true;
            this.avgHeartRateValue.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.avgHeartRateValue.Location = new System.Drawing.Point(204, 68);
            this.avgHeartRateValue.Name = "avgHeartRateValue";
            this.avgHeartRateValue.Size = new System.Drawing.Size(22, 22);
            this.avgHeartRateValue.TabIndex = 3;
            this.avgHeartRateValue.Text = "--";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(20, 68);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(161, 22);
            this.label8.TabIndex = 4;
            this.label8.Text = "Average Heart Rate :";
            // 
            // maxHeartRateValue
            // 
            this.maxHeartRateValue.AutoSize = true;
            this.maxHeartRateValue.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.maxHeartRateValue.Location = new System.Drawing.Point(204, 38);
            this.maxHeartRateValue.Name = "maxHeartRateValue";
            this.maxHeartRateValue.Size = new System.Drawing.Size(22, 22);
            this.maxHeartRateValue.TabIndex = 1;
            this.maxHeartRateValue.Text = "--";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(20, 38);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(134, 22);
            this.label5.TabIndex = 2;
            this.label5.Text = "Max Heart Rate :";
            // 
            // distanceCoveredValue
            // 
            this.distanceCoveredValue.AutoSize = true;
            this.distanceCoveredValue.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.distanceCoveredValue.Location = new System.Drawing.Point(204, 11);
            this.distanceCoveredValue.Name = "distanceCoveredValue";
            this.distanceCoveredValue.Size = new System.Drawing.Size(22, 22);
            this.distanceCoveredValue.TabIndex = 0;
            this.distanceCoveredValue.Text = "--";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(20, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(147, 22);
            this.label1.TabIndex = 0;
            this.label1.Text = "Distance Covered :";
            // 
            // hrDataGrid
            // 
            this.hrDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.hrDataGrid.Location = new System.Drawing.Point(3, 6);
            this.hrDataGrid.Name = "hrDataGrid";
            this.hrDataGrid.Size = new System.Drawing.Size(836, 542);
            this.hrDataGrid.TabIndex = 0;
            // 
            // graphPage
            // 
            this.graphPage.BackColor = System.Drawing.Color.Teal;
            this.graphPage.Controls.Add(this.label2);
            this.graphPage.Controls.Add(this.tssVal);
            this.graphPage.Controls.Add(this.ifVal);
            this.graphPage.Controls.Add(this.avgPowVal);
            this.graphPage.Controls.Add(this.avgAltVal);
            this.graphPage.Controls.Add(this.avgCadVal);
            this.graphPage.Controls.Add(this.avgSpdVal);
            this.graphPage.Controls.Add(this.avgHRVal);
            this.graphPage.Controls.Add(this.npVal);
            this.graphPage.Controls.Add(this.label21);
            this.graphPage.Controls.Add(this.label17);
            this.graphPage.Controls.Add(this.label13);
            this.graphPage.Controls.Add(this.tssLabel);
            this.graphPage.Controls.Add(this.label9);
            this.graphPage.Controls.Add(this.intesityLabel);
            this.graphPage.Controls.Add(this.label7);
            this.graphPage.Controls.Add(this.label3);
            this.graphPage.Controls.Add(this.zoomOutBtn);
            this.graphPage.Controls.Add(this.powerCheck);
            this.graphPage.Controls.Add(this.altitudeCheck);
            this.graphPage.Controls.Add(this.cadenceCheck);
            this.graphPage.Controls.Add(this.speedCheck);
            this.graphPage.Controls.Add(this.heartRateCheck);
            this.graphPage.Controls.Add(this.zgc);
            this.graphPage.Location = new System.Drawing.Point(4, 22);
            this.graphPage.Name = "graphPage";
            this.graphPage.Padding = new System.Windows.Forms.Padding(3);
            this.graphPage.Size = new System.Drawing.Size(1183, 502);
            this.graphPage.TabIndex = 2;
            this.graphPage.Text = "Graph";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Trebuchet MS", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(901, 257);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(148, 23);
            this.label2.TabIndex = 9;
            this.label2.Text = "Advance Metrics:";
            // 
            // tssVal
            // 
            this.tssVal.AutoSize = true;
            this.tssVal.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tssVal.Location = new System.Drawing.Point(1073, 363);
            this.tssVal.Name = "tssVal";
            this.tssVal.Size = new System.Drawing.Size(22, 22);
            this.tssVal.TabIndex = 8;
            this.tssVal.Text = "--";
            // 
            // ifVal
            // 
            this.ifVal.AutoSize = true;
            this.ifVal.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ifVal.Location = new System.Drawing.Point(1073, 326);
            this.ifVal.Name = "ifVal";
            this.ifVal.Size = new System.Drawing.Size(22, 22);
            this.ifVal.TabIndex = 8;
            this.ifVal.Text = "--";
            // 
            // avgPowVal
            // 
            this.avgPowVal.AutoSize = true;
            this.avgPowVal.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.avgPowVal.Location = new System.Drawing.Point(1073, 204);
            this.avgPowVal.Name = "avgPowVal";
            this.avgPowVal.Size = new System.Drawing.Size(22, 22);
            this.avgPowVal.TabIndex = 8;
            this.avgPowVal.Text = "--";
            // 
            // avgAltVal
            // 
            this.avgAltVal.AutoSize = true;
            this.avgAltVal.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.avgAltVal.Location = new System.Drawing.Point(1073, 174);
            this.avgAltVal.Name = "avgAltVal";
            this.avgAltVal.Size = new System.Drawing.Size(22, 22);
            this.avgAltVal.TabIndex = 8;
            this.avgAltVal.Text = "--";
            // 
            // avgCadVal
            // 
            this.avgCadVal.AutoSize = true;
            this.avgCadVal.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.avgCadVal.Location = new System.Drawing.Point(1073, 140);
            this.avgCadVal.Name = "avgCadVal";
            this.avgCadVal.Size = new System.Drawing.Size(22, 22);
            this.avgCadVal.TabIndex = 8;
            this.avgCadVal.Text = "--";
            // 
            // avgSpdVal
            // 
            this.avgSpdVal.AutoSize = true;
            this.avgSpdVal.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.avgSpdVal.Location = new System.Drawing.Point(1073, 103);
            this.avgSpdVal.Name = "avgSpdVal";
            this.avgSpdVal.Size = new System.Drawing.Size(22, 22);
            this.avgSpdVal.TabIndex = 8;
            this.avgSpdVal.Text = "--";
            // 
            // avgHRVal
            // 
            this.avgHRVal.AutoSize = true;
            this.avgHRVal.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.avgHRVal.Location = new System.Drawing.Point(1073, 68);
            this.avgHRVal.Name = "avgHRVal";
            this.avgHRVal.Size = new System.Drawing.Size(22, 22);
            this.avgHRVal.TabIndex = 8;
            this.avgHRVal.Text = "--";
            // 
            // npVal
            // 
            this.npVal.AutoSize = true;
            this.npVal.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.npVal.Location = new System.Drawing.Point(1073, 291);
            this.npVal.Name = "npVal";
            this.npVal.Size = new System.Drawing.Size(22, 22);
            this.npVal.TabIndex = 8;
            this.npVal.Text = "--";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(901, 204);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(125, 22);
            this.label21.TabIndex = 7;
            this.label21.Text = "Average Power:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(901, 174);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(135, 22);
            this.label17.TabIndex = 7;
            this.label17.Text = "Average Altitude:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(901, 140);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(141, 22);
            this.label13.TabIndex = 7;
            this.label13.Text = "Average Cadence:";
            // 
            // tssLabel
            // 
            this.tssLabel.AutoSize = true;
            this.tssLabel.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tssLabel.Location = new System.Drawing.Point(901, 363);
            this.tssLabel.Name = "tssLabel";
            this.tssLabel.Size = new System.Drawing.Size(164, 22);
            this.tssLabel.TabIndex = 7;
            this.tssLabel.Text = "Training Stress Score:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(901, 103);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(122, 22);
            this.label9.TabIndex = 7;
            this.label9.Text = "Average Speed:";
            // 
            // intesityLabel
            // 
            this.intesityLabel.AutoSize = true;
            this.intesityLabel.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.intesityLabel.Location = new System.Drawing.Point(901, 326);
            this.intesityLabel.Name = "intesityLabel";
            this.intesityLabel.Size = new System.Drawing.Size(129, 22);
            this.intesityLabel.TabIndex = 7;
            this.intesityLabel.Text = "Intensity Factor:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(901, 68);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(157, 22);
            this.label7.TabIndex = 7;
            this.label7.Text = "Average Heart Rate:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(901, 291);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(150, 22);
            this.label3.TabIndex = 7;
            this.label3.Text = "Normalized Power:";
            // 
            // zoomOutBtn
            // 
            this.zoomOutBtn.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.zoomOutBtn.Location = new System.Drawing.Point(1034, 13);
            this.zoomOutBtn.Name = "zoomOutBtn";
            this.zoomOutBtn.Size = new System.Drawing.Size(135, 35);
            this.zoomOutBtn.TabIndex = 6;
            this.zoomOutBtn.Text = "Zoom Out";
            this.zoomOutBtn.UseVisualStyleBackColor = true;
            this.zoomOutBtn.Click += new System.EventHandler(this.zoomOutBtn_Click);
            // 
            // powerCheck
            // 
            this.powerCheck.AutoSize = true;
            this.powerCheck.Checked = true;
            this.powerCheck.CheckState = System.Windows.Forms.CheckState.Checked;
            this.powerCheck.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.powerCheck.Location = new System.Drawing.Point(437, 19);
            this.powerCheck.Name = "powerCheck";
            this.powerCheck.Size = new System.Drawing.Size(76, 26);
            this.powerCheck.TabIndex = 5;
            this.powerCheck.Text = "Power";
            this.powerCheck.UseVisualStyleBackColor = true;
            this.powerCheck.CheckedChanged += new System.EventHandler(this.powerCheck_CheckedChanged);
            // 
            // altitudeCheck
            // 
            this.altitudeCheck.AutoSize = true;
            this.altitudeCheck.Checked = true;
            this.altitudeCheck.CheckState = System.Windows.Forms.CheckState.Checked;
            this.altitudeCheck.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.altitudeCheck.Location = new System.Drawing.Point(331, 19);
            this.altitudeCheck.Name = "altitudeCheck";
            this.altitudeCheck.Size = new System.Drawing.Size(86, 26);
            this.altitudeCheck.TabIndex = 4;
            this.altitudeCheck.Text = "Altitude";
            this.altitudeCheck.UseVisualStyleBackColor = true;
            this.altitudeCheck.CheckedChanged += new System.EventHandler(this.altitudeCheck_CheckedChanged);
            // 
            // cadenceCheck
            // 
            this.cadenceCheck.AutoSize = true;
            this.cadenceCheck.Checked = true;
            this.cadenceCheck.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cadenceCheck.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cadenceCheck.Location = new System.Drawing.Point(228, 19);
            this.cadenceCheck.Name = "cadenceCheck";
            this.cadenceCheck.Size = new System.Drawing.Size(92, 26);
            this.cadenceCheck.TabIndex = 3;
            this.cadenceCheck.Text = "Cadence";
            this.cadenceCheck.UseVisualStyleBackColor = true;
            this.cadenceCheck.CheckedChanged += new System.EventHandler(this.cadenceCheck_CheckedChanged);
            // 
            // speedCheck
            // 
            this.speedCheck.AutoSize = true;
            this.speedCheck.Checked = true;
            this.speedCheck.CheckState = System.Windows.Forms.CheckState.Checked;
            this.speedCheck.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.speedCheck.Location = new System.Drawing.Point(138, 19);
            this.speedCheck.Name = "speedCheck";
            this.speedCheck.Size = new System.Drawing.Size(73, 26);
            this.speedCheck.TabIndex = 2;
            this.speedCheck.Text = "Speed";
            this.speedCheck.UseVisualStyleBackColor = true;
            this.speedCheck.CheckedChanged += new System.EventHandler(this.speedCheck_CheckedChanged);
            // 
            // heartRateCheck
            // 
            this.heartRateCheck.AutoSize = true;
            this.heartRateCheck.Checked = true;
            this.heartRateCheck.CheckState = System.Windows.Forms.CheckState.Checked;
            this.heartRateCheck.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.heartRateCheck.Location = new System.Drawing.Point(20, 19);
            this.heartRateCheck.Name = "heartRateCheck";
            this.heartRateCheck.Size = new System.Drawing.Size(108, 26);
            this.heartRateCheck.TabIndex = 1;
            this.heartRateCheck.Text = "Heart Rate";
            this.heartRateCheck.UseVisualStyleBackColor = true;
            this.heartRateCheck.CheckedChanged += new System.EventHandler(this.heartRateCheck_CheckedChanged);
            // 
            // zgc
            // 
            this.zgc.Location = new System.Drawing.Point(20, 68);
            this.zgc.Name = "zgc";
            this.zgc.ScrollGrace = 0D;
            this.zgc.ScrollMaxX = 0D;
            this.zgc.ScrollMaxY = 0D;
            this.zgc.ScrollMaxY2 = 0D;
            this.zgc.ScrollMinX = 0D;
            this.zgc.ScrollMinY = 0D;
            this.zgc.ScrollMinY2 = 0D;
            this.zgc.Size = new System.Drawing.Size(864, 410);
            this.zgc.TabIndex = 0;
            this.zgc.UseExtendedPrintDialog = true;
            this.zgc.MouseDownEvent += new ZedGraph.ZedGraphControl.ZedMouseEventHandler(this.zgc_MouseDownEvent);
            this.zgc.MouseUpEvent += new ZedGraph.ZedGraphControl.ZedMouseEventHandler(this.zgc_MouseUpEvent);
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.Teal;
            this.tabPage1.Controls.Add(this.changeFolderPath);
            this.tabPage1.Controls.Add(this.calenDataDetails);
            this.tabPage1.Controls.Add(this.monthCalen);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1183, 502);
            this.tabPage1.TabIndex = 3;
            this.tabPage1.Text = "Calendar";
            // 
            // changeFolderPath
            // 
            this.changeFolderPath.Location = new System.Drawing.Point(326, 248);
            this.changeFolderPath.Name = "changeFolderPath";
            this.changeFolderPath.Size = new System.Drawing.Size(107, 23);
            this.changeFolderPath.TabIndex = 2;
            this.changeFolderPath.Text = "Change Directory";
            this.changeFolderPath.UseVisualStyleBackColor = true;
            this.changeFolderPath.Click += new System.EventHandler(this.changeFolderPath_Click);
            // 
            // calenDataDetails
            // 
            this.calenDataDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.calenDataDetails.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.startTime,
            this.length,
            this.interval,
            this.load_file,
            this.filePath});
            this.calenDataDetails.Location = new System.Drawing.Point(563, 52);
            this.calenDataDetails.Name = "calenDataDetails";
            this.calenDataDetails.Size = new System.Drawing.Size(443, 386);
            this.calenDataDetails.TabIndex = 1;
            this.calenDataDetails.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.calenDataDetails_CellContentClick);
            // 
            // startTime
            // 
            this.startTime.HeaderText = "Start Time";
            this.startTime.Name = "startTime";
            // 
            // length
            // 
            this.length.HeaderText = "Length";
            this.length.Name = "length";
            // 
            // interval
            // 
            this.interval.HeaderText = "Interval";
            this.interval.Name = "interval";
            // 
            // load_file
            // 
            this.load_file.DataPropertyName = "file_path";
            this.load_file.HeaderText = "Load File";
            this.load_file.Name = "load_file";
            this.load_file.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.load_file.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.load_file.Text = "Load";
            this.load_file.UseColumnTextForLinkValue = true;
            // 
            // filePath
            // 
            this.filePath.HeaderText = "File Path";
            this.filePath.Name = "filePath";
            this.filePath.ReadOnly = true;
            this.filePath.Visible = false;
            // 
            // monthCalen
            // 
            this.monthCalen.Location = new System.Drawing.Point(277, 52);
            this.monthCalen.Name = "monthCalen";
            this.monthCalen.TabIndex = 0;
            this.monthCalen.DateSelected += new System.Windows.Forms.DateRangeEventHandler(this.monthCalen_DateSelected);
            // 
            // intervalDetectionGrid
            // 
            this.intervalDetectionGrid.BackColor = System.Drawing.Color.Teal;
            this.intervalDetectionGrid.Controls.Add(this.intervalDetectSummaryPanel);
            this.intervalDetectionGrid.Controls.Add(this.intervalsGrid);
            this.intervalDetectionGrid.Location = new System.Drawing.Point(4, 22);
            this.intervalDetectionGrid.Name = "intervalDetectionGrid";
            this.intervalDetectionGrid.Padding = new System.Windows.Forms.Padding(3);
            this.intervalDetectionGrid.Size = new System.Drawing.Size(1183, 502);
            this.intervalDetectionGrid.TabIndex = 4;
            this.intervalDetectionGrid.Text = "Interval Detection";
            // 
            // intervalDetectSummaryPanel
            // 
            this.intervalDetectSummaryPanel.AutoScroll = true;
            this.intervalDetectSummaryPanel.Location = new System.Drawing.Point(738, 35);
            this.intervalDetectSummaryPanel.Name = "intervalDetectSummaryPanel";
            this.intervalDetectSummaryPanel.Padding = new System.Windows.Forms.Padding(10);
            this.intervalDetectSummaryPanel.Size = new System.Drawing.Size(418, 427);
            this.intervalDetectSummaryPanel.TabIndex = 1;
            // 
            // intervalsGrid
            // 
            this.intervalsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.intervalsGrid.Location = new System.Drawing.Point(36, 35);
            this.intervalsGrid.Name = "intervalsGrid";
            this.intervalsGrid.Size = new System.Drawing.Size(671, 427);
            this.intervalsGrid.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(1190, 587);
            this.Controls.Add(this.mainTabControl);
            this.Controls.Add(this.openFileButton);
            this.Name = "Form1";
            this.Text = "Test";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.mainTabControl.ResumeLayout(false);
            this.headersPage.ResumeLayout(false);
            this.headersPage.PerformLayout();
            this.hrDataPage.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.hrDataGrid)).EndInit();
            this.graphPage.ResumeLayout(false);
            this.graphPage.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.calenDataDetails)).EndInit();
            this.intervalDetectionGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.intervalsGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button openFileButton;
        private System.Windows.Forms.OpenFileDialog fileChooserDialog;
        private System.Windows.Forms.TabControl mainTabControl;
        private System.Windows.Forms.TabPage headersPage;
        private System.Windows.Forms.TabPage hrDataPage;
        private System.Windows.Forms.TabPage graphPage;
        private System.Windows.Forms.Label weightValue;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label vo2maxValue;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label startDelayValue;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label restHRValue;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label maxHRValue;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label activeLimitValue;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label timer3Value;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label timer2Value;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label timer1Value;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label lowerLimit3Value;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label upperLimit3Value;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label lowerLimit2Value;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label upperLimit2Value;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label lowerLimit1Value;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label upperLimit1Value;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label intervalValue;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lengthValue;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label startTimeValue;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label dateValue;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label smodeValue;
        private System.Windows.Forms.Label smodeLabel;
        private System.Windows.Forms.Label monitorValue;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label versionValue;
        private System.Windows.Forms.Label versionLabel;
        private System.Windows.Forms.HelpProvider helpProvider1;
        private System.Windows.Forms.DataGridView hrDataGrid;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label minPowerValue;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label avgPowerValue;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label maxPowerValue;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label minAltitudeValue;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label avgAltitudeValue;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label maxAltitudeValue;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label minCadenceValue;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label avgCadenceValue;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label maxCadenceValue;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label minSpeedValue;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label avgSpeedValue;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label maxSpeedValue;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label minHeartRateValue;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label avgHeartRateValue;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label maxHeartRateValue;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label distanceCoveredValue;
        private System.Windows.Forms.Label label1;
        private ZedGraph.ZedGraphControl zgc;
        private System.Windows.Forms.CheckBox powerCheck;
        private System.Windows.Forms.CheckBox altitudeCheck;
        private System.Windows.Forms.CheckBox cadenceCheck;
        private System.Windows.Forms.CheckBox speedCheck;
        private System.Windows.Forms.CheckBox heartRateCheck;
        private System.Windows.Forms.Button zoomOutBtn;
        private System.Windows.Forms.Label tssVal;
        private System.Windows.Forms.Label ifVal;
        private System.Windows.Forms.Label npVal;
        private System.Windows.Forms.Label tssLabel;
        private System.Windows.Forms.Label intesityLabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.MonthCalendar monthCalen;
        private System.Windows.Forms.DataGridView calenDataDetails;
        private System.Windows.Forms.DataGridViewTextBoxColumn startTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn length;
        private System.Windows.Forms.DataGridViewTextBoxColumn interval;
        private System.Windows.Forms.DataGridViewLinkColumn load_file;
        private System.Windows.Forms.DataGridViewTextBoxColumn filePath;
        private System.Windows.Forms.Button changeFolderPath;
        private System.Windows.Forms.TabPage intervalDetectionGrid;
        private System.Windows.Forms.DataGridView intervalsGrid;
        private System.Windows.Forms.Label avgPowVal;
        private System.Windows.Forms.Label avgAltVal;
        private System.Windows.Forms.Label avgCadVal;
        private System.Windows.Forms.Label avgSpdVal;
        private System.Windows.Forms.Label avgHRVal;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel intervalDetectSummaryPanel;
    }
}