﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZedGraph;

namespace windows_form
{
    public partial class Form1 : MaterialSkin.Controls.MaterialForm
    {
        Dictionary<string, string> param; // for storing params data 
        List<HrData> hrDataList; // for storing heart rate data
        List<Entry> calenEntries;

        double maxHR, avgHR, minHR, maxSpeed, avgSpeed, minSpeed, maxCadence, avgCadence, minCadence, maxAltitude, avgAltitude, minAltitude, maxPower, avgPower, minPower, ftp, thresoldPower, normalizedPower, intensityFactor, trainingStressScore;

        bool isHeartRateAvailable, isSpeedAvailable, isCadenceAvailable, isAltitudeAvailable, isPowerAvailable;

        GraphPane gp;

        //Index Selection
        int selectionStartIndex;
        int selectionEndIndex;

        bool graphSelected = false;

        bool isFirstTimeLoaded = true; // to store if the data is loaded for first time
        bool isDataLoaded = false;

        LineItem hrCurve, cadenceCurve, speedCurve, altitudeCurve, powerCurve; // curves for drawing graph

        string[] columnNames = new string[] { "Heart Rate", "Speed", "Cadence", "Altitude", "Power", "Time" };


        public Form1()
        {
            param = new Dictionary<string, string>();
            hrDataList = new List<HrData>();//HRData List
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)//Afyer Loading Form1
        {
            //Choose file button selection of files
            fileChooserDialog.FileName = null;
            fileChooserDialog.Title = "Select a File";

            ProcessCalendarDataEntry("Data/");
        }

        private void openFileButton_Click(object sender, EventArgs e)
        {
            if (fileChooserDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string filePath = fileChooserDialog.FileName;//reads the choosen file

                if (Path.GetExtension(filePath) != ".hrm")//see if the file is in .hrm format
                {
                    MessageBox.Show("Only .hrm files are accepted!!");//if files are not in hrm format then display this messege
                    return;
                }

                clearAllData(); // clear all data before loading another data

                readFile(filePath); // read the data from the file

                isDataLoaded = true;

                calculateHRDataSummaryValues(); // calculate summary of heart data

                showAllData(); // diplay headers, heart rate data and its summary and graph

                isFirstTimeLoaded = false;
            }
        }

        private void readFile(string filePath)//read file function which reads header
        {
            string header = null;
            string[] lines = System.IO.File.ReadAllLines(filePath); // read all the files

            // Display the file contents by using a foreach loop.

            foreach (string line in lines)
            {

                if (line.StartsWith("[") && line.EndsWith("]"))
                {
                    header = Regex.Match(line, @"(?<=\[)(.*?)(?=\])", RegexOptions.Singleline | RegexOptions.IgnoreCase).ToString();
                    continue;
                }

                switch (header)
                {
                    case "Params":
                        {
                            try
                            {
                                var parameter = line.Split('=');
                                param.Add(parameter[0], parameter[1]);

                                isHeartRateAvailable = true;

                                if (parameter[0] == "SMode")
                                {
                                    string smode = parameter[1];

                                    isSpeedAvailable = (smode[0] == '1');
                                    isCadenceAvailable = (smode[1] == '1');
                                    isAltitudeAvailable = (smode[2] == '1');
                                    isPowerAvailable = (smode[3] == '1');
                                }

                                if (parameter[0] == "Mode")
                                {
                                    string mode = parameter[1];

                                    isSpeedAvailable = true;
                                    isCadenceAvailable = (mode[0] == '0');
                                    isAltitudeAvailable = (mode[0] == '1');
                                    isPowerAvailable = false;

                                }
                            }
                            catch (Exception ex)
                            {
                                continue;
                            }
                        }
                        break;

                    case "HRData":
                        {
                            setHrData(line);
                        }
                        break;
                }
            }
        }

        // adding HrData
        private void setHrData(string data)
        {
            HrData tempHrData = new HrData();
            string smode = param["SMode"];//assigning smode 

            var hrdataArray = data.Split('\t');

            if (isHeartRateAvailable)
                tempHrData.heart_rate = int.Parse(hrdataArray[0]);//adding data in heart rate

            if (isSpeedAvailable)
                tempHrData.speed = int.Parse(hrdataArray[1]);//adding data in speed aaray

            if (isCadenceAvailable)
                tempHrData.cadence = int.Parse(hrdataArray[2]);//Adding cadence in array

            if (isAltitudeAvailable)
                tempHrData.altitude = int.Parse(hrdataArray[3]);//adding altitude in array

            if (isPowerAvailable)
                tempHrData.power = int.Parse(hrdataArray[4]);//adding power in array


            if (param["Version"] == "107" && smode[8] == '1') // add air pressure if available
            {
                tempHrData.air_pressure = int.Parse(hrdataArray[6]);
            }

            hrDataList.Add(tempHrData);
        }

        // check if euro unit is used
        private bool isEuroUnitUsed()
        {
            bool isEuroUnit = false;

            switch (param["Version"])
            {
                case "105":
                    {
                        isEuroUnit = (param["Mode"][2] == '0');//if value 2 in smode is 0 then is Euro Unit
                    }
                    break;

                case "106":
                case "107":
                    {
                        isEuroUnit = (param["SMode"][7] == '0');
                    }
                    break;
            }

            return isEuroUnit;
        }

        // returns the formatted date
        public DateTime getDate()
        {
            return new DateTime(int.Parse(param["Date"].Substring(0, 4)), int.Parse(param["Date"].Substring(4, 2)), int.Parse(param["Date"].Substring(6, 2)));
        }

        // returns distance covered
        private double getDistanceCovered()
        {
            double distance = 0;
            int interval = int.Parse(param["Interval"]);

            for (int i = 0; i < hrDataList.Count; i++)
            {
                double result = (((double)hrDataList[i].speed / 10) / 3600) * interval;

                distance += result;
            }

            return distance;
        }

        // display all data
        private void showAllData()
        {
            displayHeadersDetails();
            displayHeartRateData();
            displayHRDataSummary();
            displayGraph();
            CalcAndDisplayAdvanceMetrics(hrDataList);
            DetectIntervals();
        }

        // clear param, hr data and graph
        private void clearAllData()
        {
            if (!isFirstTimeLoaded)
            {
                param = new Dictionary<string, string>();
                hrDataList = new List<HrData>();

                if (hrCurve != null)
                    hrCurve.Clear();

                if (speedCurve != null)
                    speedCurve.Clear();

                if (altitudeCurve != null)
                    altitudeCurve.Clear();

                if (cadenceCurve != null)
                    cadenceCurve.Clear();

                if (powerCurve != null)
                    powerCurve.Clear();

                zgc.GraphPane.GraphObjList.Clear();
                zgc.GraphPane.CurveList.Clear();
                zgc.GraphPane.YAxisList.Clear();
                zgc.Invalidate();

                maxHR = 0;
                avgHR = 0;
                minHR = 0;
                maxSpeed = 0;
                avgSpeed = 0;
                minSpeed = 0;
                maxCadence = 0;
                avgCadence = 0;
                minCadence = 0;
                maxAltitude = 0;
                avgAltitude = 0;
                minAltitude = 0;
                maxPower = 0;
                avgPower = 0;
                minPower = 0;

                maxHeartRateValue.Text = " ";
                avgHeartRateValue.Text = " ";
                minHeartRateValue.Text = " ";

                maxSpeedValue.Text = " ";
                avgSpeedValue.Text = " ";
                minSpeedValue.Text = " ";

                distanceCoveredValue.Text = " ";

                maxCadenceValue.Text = " ";
                avgCadenceValue.Text = " ";
                minCadenceValue.Text = " ";

                maxAltitudeValue.Text = " ";
                avgAltitudeValue.Text = " ";
                minAltitudeValue.Text = " ";

                maxPowerValue.Text = " ";
                avgPowerValue.Text = " ";
                minPowerValue.Text = " ";

                heartRateCheck.Checked = true;
                speedCheck.Checked = true;
                cadenceCheck.Checked = true;
                altitudeCheck.Checked = true;
                powerCheck.Checked = true;
            }
        }

        // Calculating max, min and average of HRdata
        private void calculateHRDataSummaryValues()
        {
            int totalData = hrDataList.Count;

            int sumHeartRate = 0;
            int sumSpeed = 0;
            int sumCadence = 0;
            int sumAltitude = 0;
            int sumPower = 0;

            minHR = hrDataList[0].heart_rate;
            minSpeed = hrDataList[0].speed;
            minCadence = hrDataList[0].cadence;
            minAltitude = hrDataList[0].altitude;
            minPower = hrDataList[0].power;

            foreach (HrData hr in hrDataList)
            {
                if (maxHR < hr.heart_rate) maxHR = hr.heart_rate;

                if (minHR > hr.heart_rate) minHR = hr.heart_rate;

                if (maxSpeed < hr.speed) maxSpeed = hr.speed;

                if (minSpeed > hr.speed) minSpeed = hr.speed;

                if (maxCadence < hr.cadence) maxCadence = hr.cadence;

                if (minCadence > hr.cadence) minCadence = hr.cadence;

                if (maxAltitude < hr.altitude) maxAltitude = hr.altitude;

                if (minAltitude > hr.altitude) minAltitude = hr.altitude;

                if (maxPower < hr.power) maxPower = hr.power;

                if (minPower > hr.power) minPower = hr.power;

                sumHeartRate += hr.heart_rate;
                sumSpeed += hr.speed;
                sumCadence += hr.cadence;
                sumAltitude += hr.altitude;
                sumPower += hr.power;
            }

            avgHR = sumHeartRate / totalData;
            avgSpeed = sumSpeed / totalData;
            avgCadence = sumCadence / totalData;
            avgAltitude = sumAltitude / totalData;
            avgPower = sumPower / totalData;
        }

        // showing header details
        private void displayHeadersDetails()
        {
            versionValue.Text = (float.Parse(param["Version"]) / 100) + "";
            monitorValue.Text = param["Monitor"];

            if (versionValue.Text == "1.05")
            {
                smodeLabel.Text = "Mode :";
                smodeValue.Text = param["Mode"];
            }
            else
            {
                smodeLabel.Text = "SMode :";
                smodeValue.Text = param["SMode"];
            }

            dateValue.Text = getDate().ToLongDateString();
            startTimeValue.Text = param["StartTime"];
            lengthValue.Text = param["Length"];
            intervalValue.Text = param["Interval"];

            upperLimit1Value.Text = param["Upper1"] + " bpm";
            upperLimit2Value.Text = param["Upper2"] + " bpm";
            upperLimit3Value.Text = param["Upper3"] + " bpm";

            lowerLimit1Value.Text = param["Lower1"] + " bpm";
            lowerLimit2Value.Text = param["Lower2"] + " bpm";
            lowerLimit3Value.Text = param["Lower3"] + " bpm";

            timer1Value.Text = param["Timer1"];
            timer2Value.Text = param["Timer2"];
            timer3Value.Text = param["Timer3"];

            activeLimitValue.Text = param["ActiveLimit"];
            maxHRValue.Text = param["MaxHR"] + " bpm";
            restHRValue.Text = param["RestHR"] + " bpm";
            startDelayValue.Text = param["StartDelay"] + " ms";
            vo2maxValue.Text = param["VO2max"];
            weightValue.Text = param["Weight"] + " kg";
        }

        // showing hrdata in data grid view
        private void displayHeartRateData()
        {
            DateTime dtime;

            DataTable dt = new DataTable();

            string date = param["Date"];
            string[] timesArr = param["StartTime"].Split(':');

            int year = Convert.ToInt32(date.Substring(0, 4));
            int month = Convert.ToInt32(date.Substring(4, 2));
            int day = Convert.ToInt32(date.Substring(6, 2));

            int hour = Convert.ToInt32(timesArr[0]);
            int minutes = Convert.ToInt32(timesArr[1]);
            int seconds = (int)Convert.ToDouble(timesArr[2]);

            dtime = new DateTime(year, month, day, hour, minutes, seconds);

            int counter = 0;
            int interval = int.Parse(intervalValue.Text);

            foreach (string col in columnNames)
            {
                dt.Columns.Add(col);
            }

            foreach (HrData hr in hrDataList)
            {
                DataRow dr = dt.NewRow();

                dr[0] = hr.heart_rate;
                dr[1] = hr.speed;
                dr[2] = hr.cadence;
                dr[3] = hr.altitude;
                dr[4] = hr.power;
                dr[5] = dtime.AddSeconds(counter * interval).ToLongTimeString();

                dt.Rows.Add(dr);

                counter++;
            }

            hrDataGrid.DataSource = dt;
        }

        // show HRdata summary
        private void displayHRDataSummary()
        {
            string speedUnit = (isEuroUnitUsed() ? " km/h" : " mph");
            string altitudeUnit = (isEuroUnitUsed() ? " meters" : " feet");

            if (isHeartRateAvailable)
            {
                maxHeartRateValue.Text = maxHR.ToString();
                avgHeartRateValue.Text = avgHR.ToString();
                minHeartRateValue.Text = minHR.ToString();
            }

            if (isSpeedAvailable)
            {
                maxSpeedValue.Text = (maxSpeed / 10).ToString() + speedUnit;
                avgSpeedValue.Text = (avgSpeed / 10).ToString() + speedUnit;
                minSpeedValue.Text = (minSpeed / 10).ToString() + speedUnit;

                distanceCoveredValue.Text = Math.Round(getDistanceCovered()) + " " + (isEuroUnitUsed() ? "km" : "miles");
            }

            if (isCadenceAvailable)
            {
                maxCadenceValue.Text = maxCadence.ToString();
                avgCadenceValue.Text = avgCadence.ToString();
                minCadenceValue.Text = minCadence.ToString();
            }

            if (isAltitudeAvailable)
            {
                maxAltitudeValue.Text = maxAltitude.ToString() + altitudeUnit;
                avgAltitudeValue.Text = avgAltitude.ToString() + altitudeUnit;
                minAltitudeValue.Text = minAltitude.ToString() + altitudeUnit;
            }

            if (isPowerAvailable)
            {
                maxPowerValue.Text = maxPower.ToString() + " watts";
                avgPowerValue.Text = avgPower.ToString() + " watts";
                minPowerValue.Text = minPower.ToString() + " watts";
            }
        }

        // Drawing Graph
        private void displayGraph()
        {
            gp = zgc.GraphPane;

            zgc.IsZoomOnMouseCenter = true;
            zgc.IsEnableHZoom = true;
            zgc.IsEnableVZoom = false;

            gp.Chart.Border.IsVisible = false;
            gp.Title.Text = "Graph";
            gp.XAxis.Title.Text = "Time (minutes)";
            gp.IsAlignGrids = true;

            int heartRateAxis = 0;
            int speedAxis = 0;
            int cadenceAxis = 0;
            int altitudeAxis = 0;
            int powerAxis = 0;

            PointPairList hrPointList = new PointPairList();
            PointPairList cadencePointList = new PointPairList();
            PointPairList speedPointList = new PointPairList();
            PointPairList altitudePointList = new PointPairList();
            PointPairList powerPointList = new PointPairList();

            if (isHeartRateAvailable)
            {
                heartRateAxis = gp.AddYAxis("Heart Rate");
                gp.YAxisList[heartRateAxis].Color = Color.Red;
                gp.YAxisList[heartRateAxis].Scale.Max = maxHR;
                gp.YAxisList[heartRateAxis].Scale.Min = minHR;
            }

            if (isSpeedAvailable)
            {
                speedAxis = gp.AddYAxis("Speed");
                gp.YAxisList[speedAxis].Color = Color.Green;
                gp.YAxisList[speedAxis].Scale.Max = maxSpeed;
                gp.YAxisList[speedAxis].Scale.Min = minSpeed;
            }

            if (isCadenceAvailable)
            {
                cadenceAxis = gp.AddYAxis("Cadence");
                gp.YAxisList[cadenceAxis].Color = Color.Purple;
                gp.YAxisList[cadenceAxis].Scale.Max = maxCadence;
                gp.YAxisList[cadenceAxis].Scale.Min = minCadence;
            }

            if (isAltitudeAvailable)
            {
                altitudeAxis = gp.AddYAxis("Altitude");
                gp.YAxisList[altitudeAxis].Color = Color.Blue;
                gp.YAxisList[altitudeAxis].Scale.Max = maxAltitude;
                gp.YAxisList[altitudeAxis].Scale.Min = minAltitude;
            }

            if (isPowerAvailable)
            {
                powerAxis = gp.AddYAxis("Power");
                gp.YAxisList[powerAxis].Color = Color.Orange;
                gp.YAxisList[powerAxis].Scale.Max = maxPower;
                gp.YAxisList[powerAxis].Scale.Min = minPower;
            }

            DateTime startTime = getStartTime();

            gp.XAxis.Type = AxisType.Date;
            gp.XAxis.Scale.Format = "HH:mm:ss";
            gp.XAxis.MinorGrid.IsVisible = true;
            gp.XAxis.MajorGrid.IsVisible = true;

            gp.XAxis.Scale.Min = new XDate(startTime);
            gp.XAxis.Scale.Max = new XDate(getEndTime());
            gp.XAxis.Scale.MinorUnit = DateUnit.Second;
            gp.XAxis.Scale.MajorUnit = DateUnit.Minute;

            zgc.ScrollMinX = new XDate(startTime);
            zgc.ScrollMaxX = new XDate(getEndTime());

            if (isFirstTimeLoaded)
                gp.YAxis.IsVisible = false;

            double x;//for x axis
            int y1, y2, y3, y4, y5;//for y axis as it has different hrdatas to show
            int interval = int.Parse(intervalValue.Text);



            for (int i = 0; i < hrDataList.Count; i++)
            {
                x = (double)new XDate(startTime.AddSeconds(i * interval));

                if (isHeartRateAvailable)
                {
                    y1 = hrDataList[i].heart_rate;//Adding heart rate data in y1 axis
                    hrPointList.Add(x, y1);
                }

                if (isSpeedAvailable)
                {
                    y3 = hrDataList[i].speed;//Adding speed data in y1 axis
                    speedPointList.Add(x, y3);
                }

                if (isCadenceAvailable)
                {
                    y2 = hrDataList[i].cadence;//Adding cadence data in y1 axis
                    cadencePointList.Add(x, y2);
                }

                if (isAltitudeAvailable)
                {
                    y4 = hrDataList[i].altitude;//Adding altitude data in y1 axis
                    altitudePointList.Add(x, y4);
                }

                if (isPowerAvailable)
                {
                    y5 = hrDataList[i].power;//Adding power data in y1 axis
                    powerPointList.Add(x, y5);
                }
            }

            if (isHeartRateAvailable)
            {
                hrCurve = gp.AddCurve("Heart Rate", hrPointList, Color.Red, SymbolType.None);//Adding Heart Rate Curve
                hrCurve.YAxisIndex = heartRateAxis;
            }

            if (isSpeedAvailable)
            {
                speedCurve = gp.AddCurve("Speed", speedPointList, Color.Purple, SymbolType.None);//Adding speed Curve
                speedCurve.YAxisIndex = speedAxis;
            }

            if (isCadenceAvailable)
            {
                cadenceCurve = gp.AddCurve("Cadence", cadencePointList, Color.Green, SymbolType.None);//Adding cadence Curve
                cadenceCurve.YAxisIndex = cadenceAxis;
            }

            if (isAltitudeAvailable)
            {
                altitudeCurve = gp.AddCurve("Altitude", altitudePointList, Color.Blue, SymbolType.None);//Adding Altitude Curve
                altitudeCurve.YAxisIndex = altitudeAxis;
            }

            if (isPowerAvailable)
            {
                powerCurve = gp.AddCurve("Power", powerPointList, Color.Orange, SymbolType.None);//Adding Power Curve
                powerCurve.YAxisIndex = powerAxis;
            }

            zgc.AxisChange();
            zgc.Invalidate();
        }

        //DeTECTING INTERVALS
        private void DetectIntervals()
        {
            DateTime dtime;

            DataTable dt = new DataTable();//Adding data in datagrid view

            string speedUnit = (isEuroUnitUsed() ? " km/h" : " mph");//checking if its euro unit or not
            string altitudeUnit = (isEuroUnitUsed() ? " meters" : " feet");

            string date = param["Date"];//taking current date
            string[] timesArr = param["StartTime"].Split(':');//using start time as our initial time

            int year = Convert.ToInt32(date.Substring(0, 4));//splitting into year
            int month = Convert.ToInt32(date.Substring(4, 2));//splitting into month
            int day = Convert.ToInt32(date.Substring(6, 2));//splitting into date

            int hour = Convert.ToInt32(timesArr[0]);//splitting into hour
            int minutes = Convert.ToInt32(timesArr[1]);//splitting into min
            int seconds = (int)Convert.ToDouble(timesArr[2]);//splitting into sec

            dtime = new DateTime(year, month, day, hour, minutes, seconds);//date format

            int i = 0;
            int startIndex = 0;
            int counter = 0;
            int numIntervals = 0;
            int interval = int.Parse(intervalValue.Text);

            List<HrData> filtered;

            intervalDetectSummaryPanel.Controls.Clear();//clears panel

            dt.Columns.Add("Interval");//adding column interval

            foreach (string col in columnNames)
            {
                dt.Columns.Add(col);
            }

            foreach (HrData hr in hrDataList)
            {
                if (hr.power > thresoldPower)//if hr power is greater than TP then start loop
                {
                    if (counter == 0)//if count is 0 then start index 0
                    {
                        startIndex = i;
                    }

                    counter++;
                }
                else
                {
                    if (counter >= 10)//if count is >= 10 then start index add hrdata in row
                    {
                        numIntervals++;//add num interval
                        filtered = new List<HrData>();

                        for (int j = startIndex; j < i; j++)
                        {
                            HrData hrd = new HrData();

                            hrd.heart_rate = hrDataList[j].heart_rate;
                            hrd.speed = hrDataList[j].speed;
                            hrd.cadence = hrDataList[j].cadence;
                            hrd.altitude = hrDataList[j].altitude;
                            hrd.power = hrDataList[j].power;

                            filtered.Add(hrd);

                            DataRow dr = dt.NewRow();

                            dr[0] = "Interval " + numIntervals;
                            dr[1] = hrd.heart_rate;
                            dr[2] = hrd.speed;
                            dr[3] = hrd.cadence;
                            dr[4] = hrd.altitude;
                            dr[5] = hrd.power;
                            dr[6] = dtime.AddSeconds(j * interval).ToLongTimeString();

                            dt.Rows.Add(dr);
                        }

                        System.Windows.Forms.Label headingLabel = new System.Windows.Forms.Label();

                        headingLabel.AutoSize = true;
                        headingLabel.Font = new System.Drawing.Font("trebuchet ms", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                        headingLabel.Name = "intervalHeading" + numIntervals;
                        headingLabel.Text = "Interval " + numIntervals + ":";
                        headingLabel.Location = new Point(0, (numIntervals - 1) * 175);

                        System.Windows.Forms.Label detailsLabel = new System.Windows.Forms.Label();

                        detailsLabel.AutoSize = true;
                        detailsLabel.Font = new System.Drawing.Font("calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                        detailsLabel.Name = "detailsInterval" + numIntervals;
                        detailsLabel.Text = "Average Heart Rate: \t" + Math.Round(filtered.Average(data => data.heart_rate)) + " bpm \n"
                                            + "Average Speed: \t" + Math.Round(filtered.Average(data => data.speed)) + speedUnit + " \n"
                                            + "Average Cadence: \t" + Math.Round(filtered.Average(data => data.cadence)) + " \n"
                                            + "Average Altitude: \t" + Math.Round(filtered.Average(data => data.altitude)) + altitudeUnit + " \n"
                                            + "Average Power: \t" + Math.Round(filtered.Average(data => data.power)) + " watts \n";

                        detailsLabel.Location = new Point(0, 25 + (numIntervals - 1) * 175);

                        intervalDetectSummaryPanel.Controls.Add(headingLabel);
                        intervalDetectSummaryPanel.Controls.Add(detailsLabel);
                    }

                    counter = 0;
                }

                i++;
            }

            intervalsGrid.DataSource = dt;//adding data source in data grid view
        }

        // returns the start time
        private DateTime getStartTime()
        {
            string date = param["Date"];

            int year = Convert.ToInt32(date.Substring(0, 4));
            int month = Convert.ToInt32(date.Substring(4, 2));
            int day = Convert.ToInt32(date.Substring(6, 2));

            string time = param["StartTime"];

            string[] timeArr = time.Split(':');

            int hour = int.Parse(timeArr[0]);
            int min = int.Parse(timeArr[1]);
            int sec = (int)double.Parse(timeArr[2]);

            return new DateTime(year, month, day, hour, min, sec);
        }

        // returns the end time
        private DateTime getEndTime()
        {
            string time = param["Length"];

            string[] timeArr = time.Split(':');

            int hour = int.Parse(timeArr[0]);
            int min = int.Parse(timeArr[1]);
            int sec = (int)(double.Parse(timeArr[2]));

            return getStartTime().AddHours(hour).AddMinutes(min).AddSeconds(sec);
        }

        //Calculating Advanced Metrics
        private void CalcAndDisplayAdvanceMetrics(List<HrData> hrData)
        {
            if (!isDataLoaded)
                return;

            string speedUnit = (isEuroUnitUsed() ? " km/h" : " mph");
            string altitudeUnit = (isEuroUnitUsed() ? " meters" : " feet");

            double sumIncreasedPower = 0;
            int interval = int.Parse(param["Interval"]);//getting interval from params
            int duration = hrData.Count * interval;//calculating duration

            foreach (HrData data in hrData)
                sumIncreasedPower += Math.Pow(data.power, 4);

            normalizedPower = Math.Pow(sumIncreasedPower * ((double)interval / 3600), 0.25);//noramlized power calculated
            ftp = 0.95 * avgPower;//average power calculated
            intensityFactor = normalizedPower / ftp;//IF calculation
            trainingStressScore = (duration * normalizedPower * intensityFactor) / (ftp * 36);//TSS calculated
            thresoldPower = 1.05 * ftp;//TP calculated

            //displaying calculated metrics
            npVal.Text = Math.Round(normalizedPower, 2).ToString() + " Watts";
            ifVal.Text = Math.Round(intensityFactor, 2).ToString();
            tssVal.Text = Math.Round(trainingStressScore, 2).ToString();

            avgHRVal.Text = Math.Round(hrData.Average(data => data.heart_rate)).ToString() + " bpm";
            avgSpdVal.Text = Math.Round(hrData.Average(data => data.speed), 2).ToString() + speedUnit;
            avgCadVal.Text = Math.Round(hrData.Average(data => data.cadence)).ToString();
            avgAltVal.Text = Math.Round(hrData.Average(data => data.altitude), 2).ToString() + altitudeUnit;
            avgPowVal.Text = Math.Round(hrData.Average(data => data.power)).ToString() + " watts";
        }

        //Calendar Data Entry
        private void ProcessCalendarDataEntry(string targetDirectory)
        {
            int count = 0;

            calenEntries = new List<Entry>();//Creating list for data entry

            string[] fileEntries = Directory.GetFiles(targetDirectory);//getting directory
            DateTime[] dates = new DateTime[fileEntries.Count()];//getting dates from the file

            foreach (string fileName in fileEntries)
            {
                if (Path.GetExtension(fileName) != ".hrm")//check if file is in hrm format
                    continue;

                string[] data = File.ReadAllLines(fileName);

                Entry entry = new Entry();

                string date = data[4].Split('=')[1];//splitting date
                int year = Convert.ToInt32(date.Substring(0, 4));//into year
                int month = Convert.ToInt32(date.Substring(4, 2));//into months
                int day = Convert.ToInt32(date.Substring(6, 2));//into day

                //Adding splitted date into entry list and adding 
                entry.date = new DateTime(year, month, day);//interval plus filepath
                entry.startTime = data[5].Split('=')[1];
                entry.length = data[6].Split('=')[1];
                entry.interval = int.Parse(data[7].Split('=')[1]);
                entry.filePath = fileName;

                calenEntries.Add(entry);//calling calenEntries function

                dates[count] = entry.date;//counting dates

                count++;
            }

            monthCalen.BoldedDates = dates;//bold the file's dates
        }

       
        //Choosing data from date selected
        private void monthCalen_DateSelected(object sender, DateRangeEventArgs e)
        {
            calenDataDetails.Rows.Clear();//clearing calendar data

            foreach (Entry entry in calenEntries)
            {
                if (entry.date.Date == monthCalen.SelectionStart.Date.Date)
                {
                    DataGridViewRow row = new DataGridViewRow();
                    row.CreateCells(calenDataDetails);

                    row.Cells[0].Value = entry.startTime;
                    row.Cells[1].Value = entry.length;
                    row.Cells[2].Value = entry.interval;
                    row.Cells[4].Value = entry.filePath;

                    calenDataDetails.Rows.Add(row);
                }
            }
        }
        //Displaying calendar data details in datagrid view
        private void calenDataDetails_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string selected = calenDataDetails.CurrentCell.Value.ToString();

            if (selected == "Load")
            {
                string filePath = calenDataDetails.CurrentRow.Cells["filePath"].Value.ToString();

                isDataLoaded = true;

                clearAllData(); // clear all data before loading another data

                readFile(filePath); // read the data from the file

                calculateHRDataSummaryValues(); // calculate summary of heart data

                showAllData(); // diplay headers, heart rate data and its summary and graph

                isFirstTimeLoaded = false;

                MessageBox.Show("Data Successfully Loaded!!");

                mainTabControl.SelectedIndex = 0;
            }
        }
        //Change Directory btn
        private void changeFolderPath_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog opFd = new FolderBrowserDialog();

            DialogResult result = opFd.ShowDialog();

            if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(opFd.SelectedPath))
            {
                ProcessCalendarDataEntry(opFd.SelectedPath);
            }
            else
            {
                MessageBox.Show("Cancelled");
            }
        }
        //For graph selection is false
        private bool zgc_MouseDownEvent(ZedGraphControl sender, MouseEventArgs e)
        {
            if (graphSelected)
                return false;

            object nearestObject;
            int index;

            for (int i = 0; i < zgc.Height; i++)
            {
                this.zgc.GraphPane.FindNearestObject(new PointF(e.X, i), this.CreateGraphics(), out nearestObject, out index);

                if (nearestObject != null && nearestObject.GetType() == typeof(LineItem))
                {
                    graphSelected = true;
                    selectionStartIndex = index;
                    break;
                }
            }

            return false;
        }
        //if graph selection is true
        private bool zgc_MouseUpEvent(ZedGraphControl sender, MouseEventArgs e)
        {
            if (graphSelected)
            {
                object nearestObject;
                int index;
                graphSelected = false;

                for (int i = 0; i < zgc.Height; i++)
                {
                    this.zgc.GraphPane.FindNearestObject(new PointF(e.X, i), this.CreateGraphics(), out nearestObject, out index);//finding nearest point for calculation

                    if (nearestObject != null && nearestObject.GetType() == typeof(LineItem))//if nearest index is null then 
                    {
                        selectionEndIndex = index;
                        graphSelected = true;
                        break;
                    }
                }
            }

            if (graphSelected)
                ReloadAdvanceMetrics();//if graph is selected then call ReloadadvanceMetrics function

            graphSelected = false;

            return false;
        }
        //Reload advance metrics function
        private void ReloadAdvanceMetrics()
        {
            List<HrData> newHrData = new List<HrData>();

            for (int i = selectionStartIndex; i <= selectionEndIndex; i++)//looping until i<= Selection of the end index
                newHrData.Add(hrDataList[i]);

            CalcAndDisplayAdvanceMetrics(newHrData);//calling CalcandDisplayAdvanceMetrics
        }

        // show or hide the heart rate curve on the graph
        private void heartRateCheck_CheckedChanged(object sender, EventArgs e)
        {
            if (isHeartRateAvailable)
            {
                hrCurve.IsVisible = heartRateCheck.Checked;
                zgc.Invalidate();
            }
        }

        // show or hide the speed curve on the graph
        private void speedCheck_CheckedChanged(object sender, EventArgs e)
        {
            if (isSpeedAvailable)
            {
                speedCurve.IsVisible = speedCheck.Checked;
                zgc.Invalidate();
            }
        }

        // show or hide the cadence curve on the graph
        private void cadenceCheck_CheckedChanged(object sender, EventArgs e)
        {
            if (isCadenceAvailable)
            {
                cadenceCurve.IsVisible = cadenceCheck.Checked;
                zgc.Invalidate();
            }
        }

        // show or hide the altitude curve on the graph
        private void altitudeCheck_CheckedChanged(object sender, EventArgs e)
        {
            if (isAltitudeAvailable)
            {
                altitudeCurve.IsVisible = altitudeCheck.Checked;
                zgc.Invalidate();
            }
        }

        // show or hide the power curve on the graph
        private void powerCheck_CheckedChanged(object sender, EventArgs e)
        {
            if (isPowerAvailable)
            {
                powerCurve.IsVisible = powerCheck.Checked;
                zgc.Invalidate();
            }
        }

        // zooms out button cllick
        private void zoomOutBtn_Click(object sender, EventArgs e)
        {
            zgc.ZoomOutAll(gp);
            zgc.Invalidate();  // refresh the graph

            CalcAndDisplayAdvanceMetrics(hrDataList);
        }
    }
}
